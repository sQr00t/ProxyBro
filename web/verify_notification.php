<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

session_start();
require_once(__DIR__ ."/php/helpers.php");

add_dependancies();

$config = new Config();
$db = connect_db();
$user = get_user_from_session($db, false);
$current_route = $_SERVER["REQUEST_URI"];
$key = $_GET["key"];
$telegram = new Telegram($config->TELEGRAM_TOKEN);

$message = "";

if ($user && $key) {
    try {

        // get telegram details from alert_verify
        $query = "SELECT chatId FROM alert_verify WHERE passkey=? AND channel=? AND status=0";
        $stmt = $db->dbh->prepare($query);
        $stmt->execute([$key, "telegram"]);
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        
        if ($data === false) {
            throw new Exception("Invalid or expired link");
        }

        $chatId = $data["chatId"];
        
        // update telegramId in users table
        $query = "UPDATE users SET telegram_id=? WHERE username=? LIMIT 1";
        $stmt = $db->dbh->prepare($query);
        $stmt->execute([$chatId, $user->username]);
        
        // and change status to 1 in alert_verify
        $query = "UPDATE alert_verify SET status=1 WHERE chatId=?";
        $stmt = $db->dbh->prepare($query);
        $stmt->execute([$chatId]);

        $message = "Verification complete. You will now recieve attendance updates.";

        $telegram->sendMessage($chatId, $message);
        header("location: /dashboard.php");

    } catch (Exception $e) {
        $message = $e->getMessage();
    }


} else if (isset($key)) {
    header("location: /index.php?redirect={$current_route}");
} else {
    header("location: /");
}
?>

<?php 
    require_once(__DIR__. "/php/components/html_head.php");
    html_head("Dashboard");
?>
<body>
    <div class="container">
        <?php require(__DIR__."/php/components/header.php"); ?>

        <main id="verify-notification">
            <h2 class="heading"><?php echo $message; ?></h2>
            <div class="link">
                <a href="/">Go home</a>
            </div>

        </main>

        <?php require(__DIR__."/php/components/footer.php"); ?>
    </div>
</body>
</html>
