<?php
    session_start();
    require_once(__DIR__ ."/php/helpers.php");

    add_dependancies();
    redirect_unauthenticated();
    
    $db = connect_db();
    $user = get_user_from_session($db);
    
    $error = "";
    
    try {
        $user->telegramId = 0;
        $user->update($db);
    } catch (Exception $e) {
        $error = $e->getMessage();
    } finally {
        if ($error !== "") {
            header("location: /notifications.php?deactivation_notification_error={$error}");
        }

        header("location: /notifications.php");
    }

?>
