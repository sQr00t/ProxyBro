<?php
    session_start();
    require_once(__DIR__ ."/php/helpers.php");

    add_dependancies();
    
    $db = connect_db();
    $user = get_user_from_session($db, false);

?>

<?php 
    require_once(__DIR__. "/php/components/html_head.php");
    html_head("About");
?>
<body>
    <div class="container">
        <?php require(__DIR__."/php/components/header.php"); ?>

        <?php require(__DIR__."/php/components/footer.php"); ?>
    </div>
</body>
</html>
