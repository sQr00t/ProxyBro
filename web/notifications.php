<?php
    session_start();
    require_once(__DIR__ ."/php/helpers.php");

    add_dependancies();
    redirect_unauthenticated();
    
    $db = connect_db();
    $user = get_user_from_session($db);
?>

<?php 
    require_once(__DIR__. "/php/components/html_head.php");
    html_head("Notifications");
?>
<body>
    <div class="container">
        <?php require(__DIR__."/php/components/header.php"); ?>

        <main id="notifications">   
            <h1 class="heading">Activate Notifications</h1>

            <section id="telegram" class="channel">
                <div class="heading">
                    <div class="<?php echo ($user->telegramId) ? "indicator inactive" : "indicator active"; ?>"></div>
                    <h2>Telegram</h2>
                    <?php
                        if ($user->telegramId) {
                            echo '<a id="deactivate-btn" href="/deactivate_notification.php">
                                    <div class="button xs error">Unlink</div>
                                </a>';
                        }
                    ?>
                </div>

                <div class="steps">
                    <ol type="1">
                        <li>Send a <code>/start</code> message to the telegram bot <a href="https://t.me/proxybro_bot" target="_blank" rel="noopener">@proxybro_bot</a></li>
                        <li>The bot will send back a verification link.</li>
                        <li>Click on the link and login.</li>
                    </ol>
                </div>
            </section>
        </main>


        <?php require(__DIR__."/php/components/footer.php"); ?>
    </div>
</body>
</html>
