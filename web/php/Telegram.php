<?php
class Telegram {
    function __construct($token, $commands = []) {
        $this->token = $token;
        $this->commands = $commands;
    }

    function sendMessage($chatId, $text, $options = []) {
        $text = str_replace(".", "\\.", $text);
        $text = str_replace("=", "\\=", $text);
        $text = str_replace("_", "\\_", $text);
        return send_post("https://api.telegram.org/bot{$this->token}/sendMessage", [
            "chat_id" => $chatId,
            "text" => $text,
            "parse_mode" => isset($options["parse_mode"])? $options["parse_mode"] : "MarkdownV2",
            "disable_web_page_preview" => isset($options["disable_web_page_preview"])? $options["disable_web_page_preview"] : false,
        ]);
    }

    function setWebhook($url) {

        send_post("https://api.telegram.org/bot{$this->token}/setWebhook", [
            "url" => $url
        ]);
    }

    function recieveAndHandleMessage() {
        $inputJSON = file_get_contents('php://input');
        $input = json_decode($inputJSON);
        
        $chatId = $input->message->chat->id;
        $chatText = $input->message->text;

        if ($this->executeCommandIfExist($input->message, $chatId, $chatText) === false) {
            $this->sendGenericResponse($chatId);
        }

    }
    
     function executeCommandIfExist($message, $chatId, $text) {
        $isCommand = false;
        if (@isset($message->entities)) {
            foreach ($message->entities as $entity) {
                if ($entity->type === "bot_command") {
                    $isCommand = true;
                    $commandName = substr($text, $entity->offset, $entity->length);
                    $arguments = explode(" ", substr($text, $entity->offset + $entity->length));
                    if (array_key_exists($commandName, $this->commands)) {
                        $commandFunction = $this->commands[$commandName]["function"];
                        $commandFunction($chatId, $arguments, $this);
                        return true;
                    }
                    break;
                }
            }
        }

        if ($isCommand) {
            $this->sendMessage($chatId, "Command not recognised");
        }

        return false;
     }   
        
     function sendGenericResponse($chatId) {
        $helpString = "";
        foreach ($this->commands as $command=>$config) {
            $helpString .= "\n{$command} - {$config["description"]}";
        }
        $this->sendMessage($chatId, "Available commands are: ". $helpString);
     }
    
}