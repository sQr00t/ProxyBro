<?php
require_once(__DIR__."/helpers.php");

class User {
    function __construct() {
        // $this->dbh = $dbh;
    }

    function login($db, $username, $password) {
        try {
            $this->find($db, $username);
            if ($this->password !== $password) {
                throw new Exception("Invalid credentials");
            }
        } catch (Exception $e) {
            throw new Exception("Invalid credentials");
        }
            
        $_SESSION["username"] = $username;
        return true;

    }

    // update all other than username and password
    function update($db) {
        // Prepare UPDATE statement.
        $update = "UPDATE users SET active=:active, telegram_id=:telegramId WHERE username=:username";
        $stmt = $db->dbh->prepare($update);
        
        $stmt->bindParam(":username", $this->username, PDO::PARAM_STR);
        $stmt->bindParam(":active", $this->active, PDO::PARAM_STR);
        $stmt->bindParam(":telegramId", $this->telegramId, PDO::PARAM_STR);
        $result = $stmt->execute();
        
        if ($result === false) {
            throw new Exception("Couldn't update data");
            return false;
        }

        return true;
    }

    function find($db, $username) {
        // Prepare SELECT statement.
        $select = "SELECT username, name, password, active, telegram_id FROM users WHERE username=?";
        $stmt = $db->dbh->prepare($select);

        if ($db->isError()) {
            throw new Exception($db->error);
        }
        
        // Execute statement.
        $stmt->execute([$username]);
        
        // Get the results.
        $results = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($results === false) {
            throw new Exception("Cannot find user {$username}");
            return false;
        }
        $this->username = $results["username"];
        $this->name = $results["name"];
        $this->password = $results["password"];
        $this->active = intval($results["active"]);
        $this->telegramId = intval($results["telegram_id"]) === 0 ? null : $results["telegram_id"];
        return true;
    }

    function save($db) {
        $name = eduserver_login($this->username, $this->password);
        if ($name === false) {
            throw new Exception("Invalid eduserver credentials");
        }

        $insert = "INSERT INTO users (username, name, password) VALUES (:username, :name, :password)";
        $stmt = $db->dbh->prepare($insert);
        
        $stmt->bindParam(":username", $this->username, PDO::PARAM_STR);
        $stmt->bindParam(":name", $name, PDO::PARAM_STR);
        $stmt->bindParam(":password", $this->password, PDO::PARAM_STR);
        $stmt->execute();
        if ($db->isError() !== false) {
            throw new Exception("Couldn\"t create account");
        }
    }
}