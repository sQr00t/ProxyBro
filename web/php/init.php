<?php

require_once(__DIR__ . "/helpers.php");

add_dependancies();

$config = new Config();
$BASE_URL = __DIR__ ."/..";

// check if the configuration variables exist
show("\nChecking configuration variables");
$config_keys = json_decode(file_get_contents("{$BASE_URL}/php/config.sample.json"), true);

$config_error_flag = false;
foreach ($config_keys as $key => $value) {
    if (is_null($config->$key)) {
        show_error("Configuration variable '{$key}' was not found.");
        $config_error_flag = true;
    }
}

if ($config_error_flag) {
    show_error("Some configuration variables were not found.");
    exit;
}

show_success("All configuration variables found.");

// check connection of mysql using configuration
show("\nChecking mysql connection");
try {
    $config = new Config();
    
    $path = $config->DBPATH;
    $username = $config->DBUSERNAME;
    $password = $config->DBPASSWORD;
    
    $db = new Database($path);
    $db->connect($username, $password);
    $db->create_tables();
    show_success("Connected to database and tables created.");
} catch (Exception $e) {
    show_error("Couldn't connect to database.");
}

// move the telegram endpoint to a secret location
show("\nAdding telegram endpoint");
$original_file = "php/telegram_recieve";

if ((copy("{$BASE_URL}/{$original_file}.php", "{$BASE_URL}/{$config->TELEGRAM_ENDPOINT}.php")) === false) {
    show_error("Couldn't move telegram recieving file");
    exit; 
} else {
    show_success("Added telegram recieving endpoint({$config->TELEGRAM_ENDPOINT})");
}


// add telegram webhook
show("\nAdding telegram webhook");
$telegram = new Telegram($config->TELEGRAM_TOKEN);
try {
    $telegram->setWebhook("{$config->SERVER_URL}/{$config->TELEGRAM_ENDPOINT}.php");
    show_success("Webhook added");
} catch (Exception $e) {
    show_success("Telegram webhook could not be added.");
    show("Notifications will not be sent to the user");
}


function show_error($msg) {
    show($msg, true, "ERROR");
}

function show_success($msg) {
    show($msg, true, "INFO");
}

function show($msg, $newline = true, $prefix = "") {
    $prefix = $prefix ? "{$prefix}: " : "";
    echo "{$prefix}{$msg}";
    if ($newline) {
        echo PHP_EOL;
    }
}