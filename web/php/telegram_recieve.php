<?php
session_start();
require_once(__DIR__ ."/php/helpers.php");

add_dependancies();

$config = new Config();

$startCommand = function ($chatId, $arguments, $telegram) {
    $config = new Config();
    $db = connect_db();

    $message = "";

    try {
        // check if chatId already exists in users table
        $query = "SELECT COUNT(*) as count FROM users WHERE telegram_id=?";
        $stmt = $db->dbh->prepare($query);
        $stmt->execute([$chatId]);
        
        $data = $stmt->fetch(PDO::FETCH_ASSOC);

        if (intval($data["count"]) >= 1) {
            // already a telegramId exists
            $telegram->sendMessage($chatId, "A user with using this chat as notification already exists. Cannot duplicate notifications.");
            return;
        }

        // check already active activation link exists
        $query = "SELECT passkey, chatId, channel FROM alert_verify WHERE chatId=? and status=0 and channel='telegram'";
        $stmt = $db->dbh->prepare($query);
        $stmt->execute([$chatId]);

        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        
        if (isset($data["chatId"])) {

            // a link already exists
            $pass = $data["passkey"];

        } else {
            // create a new activation link
            $query = "INSERT INTO alert_verify (passkey, chatId, channel) VALUES (?, ?, ?)";
            $stmt = $db->dbh->prepare($query);
        
            if ($db->isError() !== false) {
                throw new Exception($db->error);
            }
            
            $pass = bin2hex(random_bytes(12));
        
            $stmt->execute([$pass, $chatId, "telegram"]);
            if ($db->isError() !== false) {
                throw new Exception("Couldn't create verification link");
            } 
        
        }
        
        $link = $config->SERVER_URL . "/verify_notification.php?key=". $pass;
        $telegram->sendMessage($chatId, "Click the link to activiate notifivation\n{$link}");
        
    } catch (Exception $e) {
        
        $telegram->sendMessage($chatId, "Something went wrong at out end (token: ". session_id() ."). Please [report this issue](https://git.disroot.org/sQr00t/ProxyBro/issues).", [
            "parse_mode" => "MarkdownV2",
            "disable_web_page_preview" => true,
        ]);
        flog($e->getMessage(), __FILE__, "ERROR");
    }

};

$commands = [
    "/start" => [
    "name" => "Start",
    "description" => "Start recieveing notifications from proxybro",
    "function" => $startCommand,
]];

$telegram = new Telegram($config->TELEGRAM_TOKEN, $commands);

$telegram->recieveAndHandleMessage();