<?php

class Schedule {
    
    static $STATUS_PENDING = -1;
    static $STATUS_SUCCESS = 0;
    static $STATUS_REATTEMPT = 1;
    static $STATUS_ABORT = 2;

    public static function getCount($db, $username) {
        $result = ["pending" => 0, "success" => 0, "abort" => 0];
        $query = "SELECT COUNT(*) AS count, status FROM schedule WHERE username = :username GROUP BY status";
        $stmt = $db->dbh->prepare($query);

        if ($db->isError()) {
            throw new Exception($db->error);
        }
        
        // Execute statement.
        $stmt->execute(["username" => $username]);
        
        // Get the results.
        while ($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $status = intval($data["status"]);

            if ($status === self::$STATUS_PENDING || $status === self::$STATUS_REATTEMPT) {
                $result["pending"] += $data["count"];
            } else if ($status === self::$STATUS_SUCCESS) {
                $result["success"] += $data["count"];
            } else {
                $result["abort"] += $data["count"];
            }
        }
        return $result;
    }

    public static function get($db, $username) {
        $schedules = [];
        $today = strtotime("00:00:00");
        
        $query = "SELECT subject, time, tries, status, lastattempt FROM schedule WHERE username = :username AND day >= :today";
        $stmt = $db->dbh->prepare($query);

        if ($db->isError()) {
            throw new Exception($db->error);
        }
        
        // Execute statement.
        $stmt->execute(["username" => $username, "today" => $today]);
        
        // Get the results.
        while ($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $data["status"] = self::getStatusString(intval($data["status"]));
            $schedules[] = $data;
            
        }
        return $schedules;
    }
    
    public static function format_time($time, $format) {
        try {
            $datetime = new DateTime($time);
            return $datetime->format($format);
        } catch (Exception $e) {
            return "";
        }
    }

    public static function getStatusString($status) {
        if ($status === self::$STATUS_PENDING || $status === self::$STATUS_REATTEMPT) {
            return "pending";
        } else if ($status === self::$STATUS_SUCCESS) {
            return "success";
        } else {
            return "aborted";
        }
    }
}