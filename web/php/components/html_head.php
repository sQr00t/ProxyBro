
<?php
function html_head($title = "Proxy Bro", $extra = "") {
    echo '
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>'. $title .'</title>

            <link rel="apple-touch-icon" sizes="180x180" href="/assets/apple-touch-icon.png">
            <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon-32x32.png">
            <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon-16x16.png">
            <link rel="manifest" href="/assets/site.webmanifest">
            <link rel="shortcut icon" href="/assets/favicon.ico">
            <meta name="msapplication-TileColor" content="#131a26">
            <meta name="msapplication-config" content="/assets/browserconfig.xml">
            <meta name="theme-color" content="#131a26">

            <link href="/style/main.css" rel="stylesheet" />
            '. $extra .'
        </head>
    ';
}