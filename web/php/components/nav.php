<?php
    if (isset($_SESSION["username"])) {
        echo '
            <nav>
                <a href="/dashboard.php">Dashboard</a>
                <a href="/logout.php">Logout</a>
                <a href="https://git.disroot.org/sQr00t/ProxyBro" target="_blank" rel="noopener noreferrer">Contribute</a>
                </nav>';
            } else {
                echo '
                <nav>
                <a href="/">Home</a>
                <a href="https://git.disroot.org/sQr00t/ProxyBro" target="_blank" rel="noopener noreferrer">Contribute</a>
        </nav>';
    }
