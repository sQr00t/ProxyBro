<?php

function add_dependancies() {
  require_once(__DIR__ ."/Config.php");

  $config = new Config();
    
  if (!is_null($config->SHOW_ERROR) && $config->SHOW_ERROR === 1) {
    ini_set("display_errors", "1");
    ini_set("display_startup_errors", "1");
    error_reporting(E_ALL);
  }

  require_once(__DIR__ ."/Telegram.php");
  require_once(__DIR__ ."/Database.php");
  require_once(__DIR__ ."/User.php");
  require_once(__DIR__ ."/Schedule.php");
}

function redirect_authenticated($redirect_url = null) {
  $redirect_url = $redirect_url ?? "/dashboard.php";
  if (isset($_SESSION["username"])) {
    header("location: {$redirect_url}");
  }
}

function redirect_unauthenticated($redirect_url = null) {
  $redirect_url = $redirect_url ?? "/";
  if (!isset($_SESSION["username"])) {
    header("location: {$redirect_url}");     
  }
}

function get_user_from_session($db, $display_error = true) {
  try {
    $user = new User();
    $user->find($db, @$_SESSION["username"]);
    return $user;
  } catch (Exception $e) {
    if ($display_error) {
      echo "User not found";
      echo '<a href="logout.php">Logout</a>';
      exit;
    }
    return null;
  }
  
}

function flog($message, $file, $level) {
  $message = is_string($message) ? $message : json_encode(($message));
  file_put_contents("web.log", strtoupper($level) . "|" . substr($file, -20) ."|" . date("Y-m-d h:m:s") . "|" . session_id() . "|" . $message . "\n", FILE_APPEND);
}

function connect_db() {
  try {
    $config = new Config();
    
    $path = $config->DBPATH;
    $username = $config->DBUSERNAME;
    $password = $config->DBPASSWORD;
    
    $db = new Database($path);
    $db->connect($username, $password);
    return $db;
  } catch (Exception $e) {
    echo "Error with database <br/>";
    echo '<a href="https://git.disroot.org/sQr00t/ProxyBro/issues" target="_blank" rel="noopener noreferrer">Report an issue</a>';
    exit;
  }
}

function eduserver_login($username, $password) {
  $login_url = "https://eduserver.nitc.ac.in/login/index.php";
  $valid_login_flag = "userpicture";

  $page_data = send_get($login_url);
  $login_token = get_data_between($page_data, 'name="logintoken" value="', '"');

  $login_data = send_post($login_url, [
    "logintoken" => $login_token,
    "username" => $username,
    "password" => $password
  ]);

  
  unlink("/tmp/_cookies.txt");
  if (strpos($login_data, $valid_login_flag) !== false) {
    $name = get_data_between($login_data, '<span class="usertext mr-1">', "</span>");
    return $name;
  }

  return false;
}

function get_data_between($data, $start_flag, $end_flag) {
  $start_pos = strpos($data, $start_flag);
  if ($start_pos === false) {
    return false;
  }
  $start_pos += strlen($start_flag);
  $end_pos = strpos($data, $end_flag, $start_pos);
  
  return substr($data, $start_pos, $end_pos - $start_pos);
}

function get_all_between($data, $start_flag, $end_flag) {
  $matches = [];
  $offset = 0;
  while (strpos($data, $end_flag, $offset)) {
    $matches[] = get_data_between(substr($data, $offset), $start_flag, $end_flag);
    $offset += strpos($data, $end_flag, $offset);
  }

  return $matches;
}

function send_get($url, $data = array(), $cookie_prefix = "") {
  $data_part = (!empty($data)) ? "?" . http_build_query($data) : "";
  $url .= (substr($url, -1) === "/") ? "" : "/";

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url . $data_part);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
  curl_setopt($ch, CURLOPT_HEADER, 1);
  curl_setopt($ch, CURLOPT_COOKIEFILE, "/tmp/{$cookie_prefix}_cookies.txt");
  curl_setopt($ch, CURLOPT_COOKIEJAR, "/tmp/{$cookie_prefix}_cookies.txt");

  $server_output = curl_exec($ch);
  curl_close($ch);
  return $server_output;
}

function send_post($url, $data = array(), $cookie_prefix = "", $raw_data = null) {
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_COOKIEFILE, "/tmp/{$cookie_prefix}_cookies.txt");
  curl_setopt($ch, CURLOPT_COOKIEJAR, "/tmp/{$cookie_prefix}_cookies.txt");
  curl_setopt($ch, CURLOPT_POST, 1);
  if (is_null($raw_data)) {
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
  } else {
      curl_setopt($ch, CURLOPT_POSTFIELDS, $raw_data);
  }
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_HEADER, 1);

  $server_output = curl_exec($ch);
  curl_close($ch);
  return $server_output;
}
