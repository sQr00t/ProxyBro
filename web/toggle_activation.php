<?php
    session_start();
    require_once(__DIR__ ."/php/helpers.php");

    add_dependancies();
    redirect_unauthenticated();
    
    $db = connect_db();
    $user = get_user_from_session($db);
    
    $error = "";
    
    try {
        $user->active = $user->active === 1 ? 0 : 1;
        $user->update($db);
    } catch (Exception $e) {
        $error = $e->getMessage();
    } finally {
        if ($error !== "") {
            header("location: /dashboard.php?deactivation_error={$error}");
        }

        header("location: /dashboard.php");
    }

?>
