<?php
    session_start();
    require_once(__DIR__ ."/php/helpers.php");

    add_dependancies();
    redirect_unauthenticated();
    
    $db = connect_db();
    $user = get_user_from_session($db);
    $count = Schedule::getCount($db, $user->username);
    $schedules = Schedule::get($db, $user->username);
?>

<?php 
    require_once(__DIR__. "/php/components/html_head.php");
    html_head("Dashboard");
?>
<body>
    <div class="container">
        <?php require(__DIR__."/php/components/header.php"); ?>

        <main id="dashboard">
            <div id="stats">
                <div id="stats_left">
                    <div class="info_entry"><?php echo $count["success"]; ?><span>Success</span></div>
                    <span class="seperator"></span>
                    <div class="info_entry"><?php echo $count["abort"]; ?><span>Aborted</span></div>
                    <span class="seperator"></span>
                    <div class="info_entry"><?php echo $count["pending"]; ?><span>Pending</span></div>
                </div>
                <div id="stats_right">
                    <div class="info_entry">
                        <?php echo $user->username; ?>
                        <span><?php echo strtolower($user->name); ?></span>
                    </div>
                </div>
            </div> 
            <div id="summary">
                    <div id="summary_left">
                    <?php
                        if ($user->active) {
                            echo'
                            <div class="indicator inactive"></div>
                            <div id="bro_activity" class="info_entry">
                                Bro on Duty
                                <span>Your attendance is being checked and marked</span>
                            </div>
                            ';
                        } else {
                            echo'
                            <div class="indicator active"></div>
                            <div id="bro_activity" class="info_entry">
                                Bro is chilling
                                <span>Account is inactive</span>
                            </div>
                            ';
                        }
                    ?>
                    </div>
                <div id="summary_right">
                    <?php
                        if ($user->active) {
                            echo '<a href="/toggle_activation.php">
                                    <div id="btn_acountstatus" class="button small error">
                                        Deactivate
                                    </div>
                                  </a>';
                        } else {
                            echo '<a href="/toggle_activation.php">
                                    <div id="btn_acountstatus" class="button small accent">
                                        Activate
                                    </div>
                                  </a>';
                        }
                    ?>
                    <a href="/notifications.php">
                        <div class="button ic <?php echo ($user->telegramId) ? "accent" : "error"; ?>">
                            <svg class="icon sm" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                <path d="M10 2a6 6 0 00-6 6v3.586l-.707.707A1 1 0 004 14h12a1 1 0 00.707-1.707L16 11.586V8a6 6 0 00-6-6zM10 18a3 3 0 01-3-3h6a3 3 0 01-3 3z" />
                            </svg>
                        </div>
                    </a>
                </div>
            </div> 
            <div id="schedule">
                <h2>Active Schedules</h2> 
                <table>
                    <thead>
                      <tr>
                        <th class="course">Course</th>
                        <th>Time</th>
                        <th>Attempts</th>
                        <th>Last Try</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                     <tbody>
                      <?php
                        if (count($schedules) < 1) {
                            echo '<tr><td colspan="5">No schedules for today</td></tr>';
                        } else {
                            foreach ($schedules as $schedule) {
                                echo '
                                <tr>
                                    <td class="course">'. $schedule["subject"] .'</td>
                                    <td>'. Schedule::format_time($schedule["time"], "h:i A") .'</td>
                                    <td>'. $schedule["tries"] .'</td>
                                    <td>'. Schedule::format_time($schedule["lastattempt"], "h:i A") .'</td>
                                    <td class="'. $schedule["status"] .'">'. $schedule["status"] .'</td>
                                </tr>';
                            }
                        }
                      ?>
                    </tbody>
                </table>
            </div> 
        </main>


        <?php require(__DIR__."/php/components/footer.php"); ?>
    </div>
</body>
</html>
