<?php
    session_start();
    require_once(__DIR__ ."/php/helpers.php");

    add_dependancies();
    $redirect_url = $_GET["redirect"] ?? "/dashboard.php";
    redirect_authenticated($redirect_url);
    $db = connect_db();

    $num_users = $db->getTotalCount("users");
    $num_requests = $db->getTotalCount("schedule", "WHERE status!=".Schedule::$STATUS_PENDING);

    $error = $username = "";
    
    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        
        $user = new User();
        $username = $_POST["username"] ?? "";
        $password = $_POST["password"] ?? "";

        try {
            $user->find($db, $username);
            try {
                $user->login($db, $username, $password);
                header("location: {$redirect_url}");
            } catch (Exception $e) {
                $error = $e->getMessage();
            }
        } catch (Exception $e) {
            try {
                
                $user->username = $username;
                $user->password = $password;
                
                $user->save($db);
                $_SESSION["username"] = $username;
                header("location: {$redirect_url}");
            } catch (Exception $e) {
                $error = $e->getMessage();
            }
        }
    }

    $isError = ($error !== "");

?>

<?php 
    require_once(__DIR__. "/php/components/html_head.php");
    html_head("Proxy Bro");
?>

<body>
    <div class="container">
        <?php require(__DIR__."/php/components/header.php"); ?>

        <main id="homepage">
            <section class="part left">
                <p class="site-message">Tired of marking your attendance in eduserver? Don't worry we got your back. Just sign up here with your eduserver credentials and we will mark the attendance for you so you can enjoy the <span class="em">classes</span> without bothering about attendance. </p>

                <div class="message-card">
                    <p>Current users <span class="accent"><?php echo $num_users; ?></span> </p>
                    <p>Requests served <span class="accent"><?php echo $num_requests; ?></span> </p>
                </div>
            </section>
            <section class="part right" id="signup">
                <div>
                    <h2 class="heading">Signin or Signup</h2>
                    <span class="sub">Please note that we will be storing this password in a database as plain text. So please don't use your master or important passwords.</span>
                </div>
                <form id="form" method="post" action="/index.php?redirect=<?php echo $redirect_url; ?>">
                        <input autofocus class="input <?php echo $isError ? "error" : ""; ?>" id="username" type="text" name="username" value="<?php echo $username; ?>"placeholder="eduserver username" />
                        <input class="input <?php echo $isError ? "error" : ""; ?>" id="password" type="password" name="password" placeholder="eduserver password" />
                        <div class="error"><?php echo $error; ?></div>
                    <div class="bottom-container">
                        <button type="submit" class="button accent">Enter</button>
                    </div>
                </form>
            </section>
        </main>

        <?php require(__DIR__."/php/components/footer.php"); ?>
    </div>
</body>
</html>

