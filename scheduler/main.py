from utils import Database, Attendance
from config import DBCONFIG
from datetime import datetime, date
from scheduler import clearFailed

db = Database(DBCONFIG)
db.create_schedule_table()
sch = clearFailed()
assert sch.stderr == b'', sch.stderr
# todaystamp = int(datetime.fromisoformat(str(date.today())).timestamp())
todaystamp = int(datetime.strptime(str(date.today()), "%Y-%m-%d").timestamp())
# db.clearSchedules(todaystamp)
users = db.listusers()
for userrecord in users:
    username = userrecord[0]
    password = db.getpassword(username)
    attendance = Attendance(username, password)
    attendance.makeSchedules(db, todaystamp)

