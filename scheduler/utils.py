import os
import re
import sys
from datetime import datetime, timedelta
from http.cookiejar import LWPCookieJar
from pathlib import Path

import requests
from bs4 import BeautifulSoup

import config
import scheduler
import utils
# from db import Database
from dbmysql import Database


class Attendance:
    def __init__(self, username, password):
        self.username = username
        self.baseurl = config.BASEURL
        self.s = utils.getSession(username, password)
         
    def getCalendarPage(self, test=False):
        r = self.s.get(self.baseurl + "/calendar/view.php?view=day")
        # print("NOTICE: Using local calendarpage. This should be used only for testing") #TESTING
        # return open("../../calendarpage.html", "r").read() #TESTING
        # TODO: check for errors
        return r.text

    def makeSchedules(self,db, daystamp):
        # daystamp = str(int(daystamp) + 86400) #TESTING
        calendarpage = self.getCalendarPage()

        soup = BeautifulSoup(calendarpage, 'html.parser')
        # daystampstr = str(soup.find("a", string="Policies"))
        # daystamp = re.search(r'time\%3D(\d+)', daystampstr).group(1)

        att_blocks = soup.find_all("div", attrs={"data-event-eventtype": "attendance"})
        for block in att_blocks:
            subj_par = block.find("i", attrs={"aria-label": "Course"}).parent.parent
            subj = subj_par.find("a").contents[0]

            timeurl = block.find("a", string="Today")
            time_partial = timeurl.parent.contents[1]  # ", 11:15 PM "
            today = datetime.fromtimestamp(int(daystamp))
            time_unparsed = str(today.date()) + time_partial
            time = datetime.strptime(time_unparsed, "%Y-%m-%d, %I:%M %p ")

            attendanceurl = block.find("a", attrs={"class": "card-link"}).attrs["href"]
            cid =  re.search(r'id=(\d+)', attendanceurl).group(1)

            # print(self.username, time, cid)
            existingschedule = db.getScheduleEntry(daystamp, self.username, cid)
            if existingschedule:
                existingtime = existingschedule[2]
                if existingtime != time:
                    print("New schedule detected")
                    try:
                        sch = scheduler.cancelSchedule(self.username, cid, capture_output=False)
                        if sch.returncode != 0:
                            print("CancelSchedule Failed stdout: {} stderr: {}".format(sch.stdout, sch.stderr))
                        sch = scheduler.Schedule(str(time), self.username, cid, daystamp, capture_output=False)
                        if sch.returncode != 0:
                            print("SetNewSchedule Failed stdout: {} stderr: {}".format(sch.stdout, sch.stderr))
                        db.updateScheduleEntry(daystamp, self.username, cid, time) 
                    except Exception as e:
                        print("Renew failed:", e)
                else:
                    print("Same schedule detected. continue")
                    continue
            else:
                sch = scheduler.Schedule(str(time), self.username, cid, daystamp, capture_output=False)
                if sch.returncode != 0:
                    print("CancelSchedule Failed stdout: {} stderr: {}".format(sch.stdout, sch.stderr))
                    exit(1)
                db.createScheduleEntry(daystamp, self.username, cid, time, subj) 

def login_session(username, password, sess):
        # Get login token
        r = sess.get(config.BASEURL + "/login/index.php")
        pattern = r'<input type="hidden" name="logintoken" value="(\w{32})">'
        token = re.search(pattern, r.text)
        logintoken = token.group(1)
        login = {
            "username":  username,
            "password": password,
            "logintoken": logintoken
        }
        # Login with credentials
        r = sess.post(
            config.BASEURL + '/login/index.php',
            data=login
        )
        # TODO: check response status
        return sess


def notLoggedIn(s):
    r = s.get(config.BASEURL)
    return ("login" in r.url)


def getSession(username, password):
    s = requests.Session()
    cookiejar = Path(config.BASEPATH, "cookies", username + ".cjar")
    s.cookies = LWPCookieJar(str(cookiejar))
    if os.path.exists(cookiejar):
        print("Loading existing cookies for", username)
        s.cookies.load(ignore_discard=True)
    if notLoggedIn(s):
        print(username, "is not logged in. Logging in...")
        s = login_session(username, password, s)
        s.cookies.save(ignore_discard=True)
    return s

def sendTelegram(msg, chatid):
    tgurl = "https://api.telegram.org/bot{}/sendMessage".format(config.TGTOKEN)
    data = {
        "chat_id":  chatid,
        "text": msg
    }
    return requests.post(tgurl, data)
