import sqlite3
class Database:
    def __init__(self, file):
        self.conn = sqlite3.connect(file, detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)
    def listusers(self):
        c = self.conn.cursor()
        users = c.execute('SELECT username FROM users where active=1')
        return users

    def getpassword(self,username):
        c = self.conn.cursor()
        c.execute('SELECT password FROM users where username=?', (username, ))
        entry = c.fetchone()
        assert entry is not None, "Invalid User"
        return entry[0]
    def __del__(self):
        self.conn.commit()
        self.conn.close()
    def recordSchedule(self, username, cid, tries, status,ts):
        c = self.conn.cursor()

        res = c.execute(''' 
                        UPDATE schedule set tries=?, status=?, lastattempt=? 
                        WHERE username=? AND cid=?
        ''',
                        (tries, status, ts, username, cid))
        return res


    def getScheduleEntry(self, username, cid):
        c = self.conn.cursor()
        print("Username: {}, cid: {}".format(username, cid))

        #SELECT tries, status, lastattempt as "lastattempt [timestamp]"
        res = c.execute('''
                        SELECT tries, status
                        FROM schedule where username=? AND cid=?
        ''',
                        (username, cid))
        entry = c.fetchone()
        assert entry is not None, "No such user-cid pair"
        return entry
    def createScheduleEntry(self, username, cid, subj):
        c = self.conn.cursor()

        res = c.execute('INSERT INTO schedule (username, cid, subject) VALUES (?,?,?)',
                        (username, cid, subj))
        return res
    def clearSchedules(self):
        c = self.conn.cursor()
        res = c.execute('DELETE FROM schedule')
        return res
    def create_schedule_table(self):
        c = self.conn.cursor()
        # status
        # ======
        # -1: scheduled pending
        # 0: success marked
        # 1: run fail reattempt
        # 2: run abort
        query = '''
            CREATE TABLE IF NOT EXISTS schedule (
            username VARCHAR(10) NOT NULL,
            cid VARCHAR(10) NOT NULL,
            subject VARCHAR(50) NOT NULL,
            tries INTEGER NOT NULL DEFAULT 0,
            status INTEGER NOT NULL DEFAULT -1,
            lastattempt timestamp)
        '''
        res = c.execute(query)
        return res

       
