import asyncio, socket
import re
import sys
from datetime import datetime
from pathlib import Path

from bs4 import BeautifulSoup

import utils
from config import BASEPATH, BASEURL, DBCONFIG
from utils import Database, sendTelegram
db = Database(DBCONFIG)


def markAttendance(username, cid, day):
    fullurl = BASEURL + "/mod/attendance/view.php?id=" + cid
    password = db.getpassword(username)
    print(day, username, cid)
    entry = db.getScheduleEntry(day, username, cid)
    if entry is None:
        print("No such entry")
        return 2
    tries = entry[0]
    status = entry[1]
    subject = entry[3]
    chatentry = db.getChatId(username)
    chatid = chatentry[0]

    def recordSchedule(db,tries,status):
        # TODO: validate response
        dbres = db.recordSchedule(day, username, cid, tries, status)
        if chatid:
            msgdb = {
                0: "Attendance marked",
                1: "Attendance not found, will retry",
                2: "Attendance marking aborted"
            }
            tgmsg = "{}: *{}*".format(msgdb[status], subject)
            tgres = sendTelegram(tgmsg, chatid)
            if tgres.status_code != 200:
                print("Telegram Req failed:", tgres.text)
        if status == 2:
            return 0
        return status
    s = utils.getSession(username, password)
    r = s.get(fullurl)
    r_text = r.text

    pattern = r'mod\/attendance\/attendance.php\?sessid=\d+&amp;sesskey=\w+'
    searchres = re.findall(pattern, r_text)
    if len(searchres) != 1:
        print("Got {} Submission urls".format(len(searchres)))
        nooftries = 5
        if tries >= nooftries-1:
            print("Aborting - TooManyAttempts")
            return recordSchedule(db,tries+1, 2)
        else:
            return recordSchedule(db,tries+1, 1)
    submiturl = searchres[0]
    # Goto submission url to mark present,late,absent
    r = s.get(BASEURL + "/" + submiturl)
    attendance_mode = re.search(r'mod\/attendance\/(\w+)\.php', r.url).group(1)

    if attendance_mode == "view":
        print("Mark without submission. Success")
        return recordSchedule(db,tries+1, 0)

    r_text = r.text
    # r_text = open("./submit.html", "r").read()
    soup = BeautifulSoup(r_text, 'html.parser')
    # print(soup.find_all(attrs={"name": "status"}).parent)
    present_span = soup.find("span", class_="statusdesc", string="Present")
    if not present_span:
        print("Couldnt find present option. maybe late?")
        return recordSchedule(db,tries+1, 2)
    present_status = present_span.parent.find("input", attrs={"name": "status"}).attrs["value"]
    sessid = soup.find("input", attrs={"name": "sessid"}).attrs["value"]
    sesskey = soup.find("input", attrs={"name": "sesskey"}).attrs["value"]
    data = {
        "status":  present_status,
        "sessid": sessid,
        "sesskey": sesskey,
        "_qf__mod_attendance_student_attendance_form": "1",
        "mform_isexpanded_id_session": "1",
        "submitbutton": "Save+changes"
    }
    print(data)
    r = s.post(
        BASEURL + '/mod/attendance/attendance.php',
        data=data
    )
    print("Posted data")
    if r.status_code == 200:
        return recordSchedule(db,tries+1, 0)
    else:
        print("Unexpected error. will try again")
        return recordSchedule(db,tries+1, 1)
    return 2

async def handle_client(reader, writer):
    request = None
    request = (await reader.read(255)).decode('utf8')
    [rollno, cid, day] = request.split(' ')
    excode = markAttendance(rollno, cid, day)

    response = str(excode)
    writer.write(response.encode('utf8'))
    await writer.drain()
    writer.close()

loop = asyncio.get_event_loop()
loop.create_task(asyncio.start_server(handle_client, 'localhost', 15555))
try:
    loop.run_forever()
except KeyboardInterrupt:
    loop.close()
    # del db


