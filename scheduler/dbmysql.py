import sqlite3
import pymysql.cursors
class Database:
    def __init__(self, config):
        self.conn = pymysql.connect(host=config["host"],
                                    user=config["user"],
                                    password=config["password"],
                                    database=config["database"]
                                    )
    def listusers(self):
        c = self.conn.cursor()
        c.execute('SELECT username FROM users where active=1')
        users = c.fetchall()
        return users

    def getpassword(self,username):
        c = self.conn.cursor()
        c.execute('SELECT password FROM users where username=%s', (username, ))
        entry = c.fetchone()
        assert entry is not None, "Invalid User"
        return entry[0]
    def __del__(self):
        self.conn.commit()
        self.conn.close()
    def recordSchedule(self, day, username, cid, tries, status):
        c = self.conn.cursor()

        res = c.execute(''' 
                        UPDATE schedule set tries=%s, status=%s 
                        WHERE day=%s AND username=%s AND cid=%s
        ''',
                        (tries, status, day, username, cid))
        self.conn.commit()
        return res


    def getScheduleEntry(self, day, username, cid):
        c = self.conn.cursor()
        print("GET {}:{}:{}".format(day, username, cid))
        res = c.execute('''
                        SELECT tries, status, time, subject
                        FROM schedule where day=%s AND username=%s AND cid=%s
        ''',
                        (day, username, cid))
        entry = c.fetchone()
        return entry
    def createScheduleEntry(self,day, username, cid, time, subj):
        c = self.conn.cursor()

        res = c.execute('INSERT INTO schedule (day, username, cid, subject, time) VALUES (%s,%s,%s,%s,%s)',
                        (day, username, cid, subj, time))
        return res
    def updateScheduleEntry(self,day, username, cid, time):
        c = self.conn.cursor()
        res = c.execute('''
                        UPDATE schedule SET time=%s, status=-1, tries=0
                        WHERE day=%s AND username=%s AND cid=%s
        ''',
                        (time, day, username, cid))
        return res
    def clearSchedules(self, daystamp):
        c = self.conn.cursor()
        res = c.execute('DELETE FROM schedule WHERE day=%s', (daystamp, ))
        return res
    def getChatId(self, username):
        c = self.conn.cursor()
        res = c.execute('''
                        SELECT telegram_id
                        FROM users where username=%s
        ''',
                        (username))
        entry = c.fetchone()
        return entry
    def create_schedule_table(self):
        c = self.conn.cursor()
        # status
        # ======
        # -1: scheduled pending
        # 0: success marked
        # 1: run fail reattempt
        # 2: run abort
            # day TIMESTAMP NULL DEFAULT 0,
        query = '''
            CREATE TABLE IF NOT EXISTS schedule (
            day INT(10) unsigned DEFAULT 0,
            username VARCHAR(10) NOT NULL,
            cid VARCHAR(10) NOT NULL,
            subject VARCHAR(50) NOT NULL,
            tries INTEGER NOT NULL DEFAULT 0,
            status INTEGER NOT NULL DEFAULT -1,
            time TIMESTAMP NOT NULL DEFAULT 0,
            lastattempt TIMESTAMP NULL DEFAULT 0 ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY(day, username, cid))
        '''
        res = c.execute(query)
        return res

       
