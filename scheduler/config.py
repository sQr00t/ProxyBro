from os import path
from pathlib import Path
import json

BASEPATH = Path(__file__).parent.absolute()
cfg = json.loads(Path(BASEPATH, "config.json").read_text())
BASEURL = cfg["baseurl"]
DBCONFIG = cfg["database"]
TGTOKEN = cfg["telegram_token"]
# DB_FILE = Path(BASEPATH,  "..", "users.sqlite")
