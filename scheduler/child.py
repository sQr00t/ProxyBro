import socket, sys
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.connect(('localhost', 15555))

if len(sys.argv) != 4:
    print("No args given boi")
    print("child.py <user> <link> <day>")
    exit(1)

username = sys.argv[1]
cid = sys.argv[2]
day = sys.argv[3]

request = "{} {} {}".format(username, cid, day)
server.send(request.encode('utf8'))
response = server.recv(24).decode('utf8')
server.close()
excode = int(response)
exit(excode)

