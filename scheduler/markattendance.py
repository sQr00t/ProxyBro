import re
import sys
from datetime import datetime
from pathlib import Path

from bs4 import BeautifulSoup

import utils
from config import BASEPATH, BASEURL, DBCONFIG
from utils import Database, sendTelegram

# take username and url
if len(sys.argv) != 4:
    print("No args given boi")
    print("markattendance.py <user> <link> <day>")
    exit(1)

username = sys.argv[1]
cid = sys.argv[2]
day = sys.argv[3]
fullurl = BASEURL + "/mod/attendance/view.php?id=" + cid

db = Database(DBCONFIG)
password = db.getpassword(username)
entry = db.getScheduleEntry(day, username, cid)
assert entry is not None, "No such user-cid pair"
tries = entry[0]
status = entry[1]
subject = entry[3]
chatentry = db.getChatId(username)
chatid = chatentry[0]

def recordSchedule(db,tries,status):
    # TODO: validate response
    db.recordSchedule(day, username, cid, tries, status)
    if chatid:
        msgdb = {
            0: "Attendance marked",
            1: "Attendance not found, will retry",
            2: "Attendance marking aborted"
        }
        tgmsg = "{}: *{}*".format(msgdb[status], subject)
        tgres = sendTelegram(tgmsg, chatid)
        if tgres.status_code != 200:
            print("Telegram Req failed:", tgres.text)
    if status == 2:
        exit(0)
    exit(status)



# load cookie
s = utils.getSession(username, password)

# mark attendance
r = s.get(fullurl)
r_text = r.text

pattern = r'mod\/attendance\/attendance.php\?sessid=\d+&amp;sesskey=\w+'
searchres = re.findall(pattern, r_text)
if len(searchres) != 1:
    print("Got {} Submission urls".format(len(searchres)))
    nooftries = 5
    if tries >= nooftries-1:
        print("Aborting - TooManyAttempts")
        recordSchedule(db,tries+1, 2)
    else:
        recordSchedule(db,tries+1, 1)
submiturl = searchres[0]
# Goto submission url to mark present,late,absent
r = s.get(BASEURL + "/" + submiturl)
attendance_mode = re.search(r'mod\/attendance\/(\w+)\.php', r.url).group(1)

if attendance_mode == "view":
    print("Mark without submission. Success")
    recordSchedule(db,tries+1, 0)

r_text = r.text
# r_text = open("./submit.html", "r").read()
soup = BeautifulSoup(r_text, 'html.parser')
# print(soup.find_all(attrs={"name": "status"}).parent)
present_span = soup.find("span", class_="statusdesc", string="Present")
if not present_span:
    print("Couldnt find present option. maybe late?")
    recordSchedule(db,tries+1, 2)
present_status = present_span.parent.find("input", attrs={"name": "status"}).attrs["value"]
sessid = soup.find("input", attrs={"name": "sessid"}).attrs["value"]
sesskey = soup.find("input", attrs={"name": "sesskey"}).attrs["value"]
data = {
    "status":  present_status,
    "sessid": sessid,
    "sesskey": sesskey,
    "_qf__mod_attendance_student_attendance_form": "1",
    "mform_isexpanded_id_session": "1",
    "submitbutton": "Save+changes"
}
print(data)
r = s.post(
    BASEURL + '/mod/attendance/attendance.php',
    data=data
)
print("Posted data")
if r.status_code == 200:
    recordSchedule(db,tries+1, 0)
else:
    print("Unexpected error. will try again")
    recordSchedule(db,tries+1, 1)
    
