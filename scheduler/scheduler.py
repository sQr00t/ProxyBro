import subprocess
import sys

from pathlib import Path

from config import BASEPATH

#runner = str(Path(BASEPATH, "child.py"))
runner = str(Path(BASEPATH, "markattendance.py"))


def Schedule(time, user, cid, day, capture_output=True):
    command = [sys.executable, runner, user, cid, str(day)]
    servicename = "att_{}_{}".format(cid, user)

    restartIntervalMinutes = 1
    # restartAttemptCount = 5
    # burstIntervalSec = 60 * (5 + restartAttemptCount) * restartIntervalMinutes

    propResartSec = "RestartSec={}m".format(restartIntervalMinutes)
    # propStartLimitIntervalSec = "StartLimitIntervalSec={}".format(burstIntervalSec)
    # propStartLimitBurst="StartLimitBurst={}".format(restartAttemptCount)

    schedulecmd = ['systemd-run',
                   '--user',
                   '--unit', servicename,
                   '--on-calendar', time,
                   '--property', "Restart=on-failure",
                   # '--property', "RestartPreventExitStatus=2",
                   '--property', propResartSec,
                   # '--property', propStartLimitIntervalSec,
                   # '--property', propStartLimitBurst,
                   *command]
    # sch = subprocess.run(['echo', user, cid, str(day)], capture_output=capture_output) #TESTING
    sch = subprocess.run( schedulecmd, capture_output=capture_output)
    return sch
def cancelSchedule(user, cid, capture_output=True):
    servicename = "att_{}_{}.timer".format(cid, user)

    schedulecmd = ['systemctl',
                   '--user',
                   'stop', servicename]
    # sch = subprocess.run(['echo', user, cid, str(day)], capture_output=capture_output)
    sch = subprocess.run(schedulecmd, capture_output=capture_output)
    return sch
def clearFailed(capture_output=True):
    schedulecmd = ['systemctl',
                   '--user',
                   'reset-failed', 'att_*']
    # sch = subprocess.run(['echo', user, cid, str(day)], capture_output=capture_output)
    sch = subprocess.run(schedulecmd, capture_output=capture_output)
    return sch

if __name__ == "__main__":
    from datetime import datetime, timedelta
    time = datetime.now() + timedelta(seconds = 10)
    # Testing scheduler
    Schedule(str(time), "test_rollno", "28931", capture_output=False)
