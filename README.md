# ProxyBro

Mark your attendance in eduserver automatically.

## Running

This project has two parts the web server which gives the UI to enter new users and the scheduler which does all the heavy lifting and marks the attendance.

### Web server

Requires
- php >= 7
- curl
- php-curl
- mysql/postgres
- web server (only needed in production)

Run copy the configuration file from `web/php/config.sample.php` to `web/php/config.php` and change the values accordingly.
```sh
cp web/php/config.sample.json web/php/config.json
```

Run the intialization script to check and initialize the configuration.
```sh
php web/php/init.php
```

In production hide the folder `php` from the webserver. For development use, 
```sh
php -S localhost:8001 -t web

```
All is well.
